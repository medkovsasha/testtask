#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QThread>
#include <QFile>
#include <QTcpSocket>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QByteArray>

class MyThread : public QThread
{
    Q_OBJECT
public:
    explicit MyThread(qintptr ID, QObject *parent = 0);
    void run();
    void setData(const QByteArray &source);
signals:
    void error(QTcpSocket::SocketError socketerror);
    void save(QByteArray data);
    void takedata(MyThread* thread);
public slots:
    void readyRead();
    void disconnected();
private:
    QTcpSocket* socket;
    qintptr socketDescriptor;
    QByteArray threadData;
};

#endif // MYTHREAD_H
