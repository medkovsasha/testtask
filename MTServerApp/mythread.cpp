#include "mythread.h"

MyThread::MyThread(qintptr ID, QObject *parent) : QThread(parent)
{
    this->socketDescriptor = ID;
}
void MyThread::run()
{
    qDebug() << " Thread started";

    socket = new QTcpSocket();

    if(!socket->setSocketDescriptor(this->socketDescriptor))
    {
        emit error(socket->error());
        return;
    }

    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()), Qt::DirectConnection);
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));

    socket->write("{\"type\":\"connect\",\"status\":\"yes\"}");

    qDebug() << socketDescriptor << " Client connected";

    exec();
}

void MyThread::readyRead()
{
    QByteArray socketData = socket->readAll();
    QJsonParseError commandError;
    QJsonDocument clientCommand = QJsonDocument::fromJson(socketData, &commandError);
    if(commandError.errorString() == "no error occurred")
    {
        if(clientCommand.object().value("type").toString() == "takedata")
        {
            emit takedata(this);
            QByteArray toClient;
            if(threadData.isEmpty())
            {
                toClient = "{\"type\":\"fileisempty\"}";
            }
            else
            {
                toClient = "{\"type\":\"data\",\"result\":"+threadData+"}";
            }
            socket->write(toClient);
            socket->waitForBytesWritten(500);
        }
        else if(clientCommand.object().value("type").toString() == "save")
        {
            QJsonParseError docError;
            QJsonDocument doc = QJsonDocument::fromJson(socketData, &docError);
            QJsonArray contactsArray = doc.object().value("result").toArray();
            threadData = QJsonDocument(contactsArray).toJson();
            emit save(socketData);
        }
        else
        {
            qDebug()<<"Error";
        }
    }
    else
    {
        qDebug()<<"Reques error"<<commandError.errorString();
    }
}

void MyThread::disconnected()
{
    qDebug() << socketDescriptor << " Disconnected";
    socket->deleteLater();
    exit(0);
}

void MyThread::setData(const QByteArray &source)
{
    threadData = source;
}
