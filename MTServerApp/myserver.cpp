#include "myserver.h"
#include "mythread.h"

MyServer::MyServer(QObject *parent) : QTcpServer(parent)
{
    file.setFileName("contactbook.json");
    if(file.open(QIODevice::ReadWrite|QFile::Text))
    {
        serverData = file.readAll();
        file.close();
    }
    else
    {
        qDebug()<<"File error";
    }
}

void MyServer::startServer()
{
    if(this->listen(QHostAddress::Any, 5555))
    {
        qDebug()<<"LISTEN";
    }
    else
    {
        qDebug()<<"NOT LISTEN";
    }
}

void MyServer::incomingConnection(qintptr socketDescriptor)
{
    MyThread *thread = new MyThread(socketDescriptor, this);
    setThreaddata(thread);
    connect(thread, SIGNAL(takedata(MyThread*)), this, SLOT(setThreaddata(MyThread*)));
    connect(thread, SIGNAL(save(QByteArray)), this, SLOT(saveContactbook(QByteArray)));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->start();
}

void MyServer::saveContactbook(QByteArray contactbook)
{
    QJsonParseError docError;
    QJsonDocument doc = QJsonDocument::fromJson(contactbook, &docError);
    QJsonArray contactsArray = doc.object().value("result").toArray();
    serverData = QJsonDocument(contactsArray).toJson();
    file.open(QIODevice::ReadWrite|QFile::Text);
    file.resize(0);
    file.write(serverData);
    file.close();
}

void MyServer::setThreaddata(MyThread* thread)
{
    thread->setData(serverData);
}
