#ifndef MYSERVER_H
#define MYSERVER_H

#include <QTcpServer>
#include <QTcpSocket>
#include "mythread.h"

class MyServer : public QTcpServer
{
    Q_OBJECT
public:
    explicit MyServer(QObject *parent = 0);
    void startServer();
public slots:
    void saveContactbook(QByteArray contactbook);
    void setThreaddata(MyThread* thread);
protected:
    void incomingConnection(qintptr socketDescriptor);
private:
    QFile file;
    QByteArray serverData;
};

#endif // MYSERVER_H
