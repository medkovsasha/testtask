#include "deletedialog.h"

deleteDialog::deleteDialog(QWidget *pwgt) : QDialog(pwgt, Qt::WindowTitleHint | Qt::WindowSystemMenuHint)
{
    m_ptxtname = new QLineEdit;

    QLabel* plblname = new QLabel("name");

    plblname->setBuddy(m_ptxtname);

    QPushButton* pcmdOk = new QPushButton("OK");
    QPushButton* pcmdCancel = new QPushButton("Cancel");

    connect(pcmdOk, SIGNAL(clicked()), SLOT(accept()));
    connect(pcmdCancel, SIGNAL(clicked()), SLOT(reject()));

    QGridLayout* ptopLayout = new QGridLayout;
    ptopLayout->addWidget(plblname, 0, 0);
    ptopLayout->addWidget(m_ptxtname, 0, 1);
    ptopLayout->addWidget(pcmdOk, 2, 2);
    ptopLayout->addWidget(pcmdCancel, 3, 2);
    setLayout(ptopLayout);

}

QString deleteDialog::getName() const
{
    return m_ptxtname->text();
}
