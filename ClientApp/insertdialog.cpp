#include "insertdialog.h"

insertDialog::insertDialog(QWidget *pwgt) : QDialog(pwgt, Qt::WindowTitleHint | Qt::WindowSystemMenuHint)
{
    m_ptxtname = new QLineEdit;
    m_ptxtnumber = new QLineEdit;

    QLabel* plblname = new QLabel("name");
    QLabel* plblnumber = new QLabel("number");

    plblname->setBuddy(m_ptxtname);
    plblnumber->setBuddy(m_ptxtnumber);

    QPushButton* pcmdOk = new QPushButton("OK");
    QPushButton* pcmdCancel = new QPushButton("Cancel");

    connect(pcmdOk, SIGNAL(clicked()), SLOT(accept()));
    connect(pcmdCancel, SIGNAL(clicked()), SLOT(reject()));

    QGridLayout* ptopLayout = new QGridLayout;
    ptopLayout->addWidget(plblname, 0, 0);
    ptopLayout->addWidget(plblnumber, 1, 0);
    ptopLayout->addWidget(m_ptxtname, 0, 1);
    ptopLayout->addWidget(m_ptxtnumber, 1, 1);
    ptopLayout->addWidget(pcmdOk, 2, 2);
    ptopLayout->addWidget(pcmdCancel, 3, 2);
    setLayout(ptopLayout);

}

QString insertDialog::getName() const
{
    return m_ptxtname->text();
}

QString insertDialog::getNumber() const
{
    return m_ptxtnumber->text();
}
