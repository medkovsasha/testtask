#ifndef DELETEDIALOG
#define DELETEDIALOG

#include <QLabel>
#include <QWidget>
#include <QDialog>
#include <QLineEdit>
#include <QGridLayout>
#include <QString>
#include <QPushButton>

class deleteDialog : public QDialog
{
    Q_OBJECT
public:
    deleteDialog(QWidget* pwgt = 0);
    QString getName() const;
private:
    QLineEdit* m_ptxtname;
};

#endif // DELETEDIALOG

