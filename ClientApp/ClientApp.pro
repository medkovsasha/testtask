QT += core
QT -= gui
QT += network
QT += widgets

TARGET = ClientApp
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    mainwindow.cpp \
    insertdialog.cpp \
    changedialog.cpp \
    deletedialog.cpp

HEADERS += \
    mainwindow.h \
    insertdialog.h \
    changedialog.h \
    deletedialog.h

CONFIG += C++11
