#include "changedialog.h"

changeDialog::changeDialog(QWidget *pwgt) : QDialog(pwgt, Qt::WindowTitleHint | Qt::WindowSystemMenuHint)
{
    m_ptxtname = new QLineEdit;
    m_ptxtnewname = new QLineEdit;
    m_ptxtnewphonenumber = new QLineEdit;

    QLabel* plblname = new QLabel("name");
    QLabel* plblnewname = new QLabel("newname");
    QLabel* plblnewphonenumber = new QLabel("newphonenumber");

    plblname->setBuddy(m_ptxtname);
    plblnewname->setBuddy(m_ptxtnewname);
    plblnewphonenumber->setBuddy(m_ptxtnewphonenumber);

    QPushButton* pcmdOk = new QPushButton("OK");
    QPushButton* pcmdCancel = new QPushButton("Cancel");

    connect(pcmdOk, SIGNAL(clicked()), SLOT(accept()));
    connect(pcmdCancel, SIGNAL(clicked()), SLOT(reject()));

    QGridLayout* ptopLayout = new QGridLayout;
    ptopLayout->addWidget(plblname, 0, 0);
    ptopLayout->addWidget(plblnewname, 1, 0);
    ptopLayout->addWidget(plblnewphonenumber, 2, 0);
    ptopLayout->addWidget(m_ptxtname, 0, 2);
    ptopLayout->addWidget(m_ptxtnewname, 1, 2);
    ptopLayout->addWidget(m_ptxtnewphonenumber, 2, 2);
    ptopLayout->addWidget(pcmdOk, 0, 3);
    ptopLayout->addWidget(pcmdCancel, 1, 3);
    setLayout(ptopLayout);

}

QString changeDialog::getName() const
{
    return m_ptxtname->text();
}

QString changeDialog::getnewName() const
{
    return m_ptxtnewname->text();
}

QString changeDialog::getnewPhonenumber() const
{
    return m_ptxtnewphonenumber->text();
}
