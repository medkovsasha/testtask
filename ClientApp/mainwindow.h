#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QTcpSocket>
#include <QTableWidget>
#include <QPushButton>
#include <QMessageBox>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonParseError>
#include "deletedialog.h"
#include "changedialog.h"
#include "insertdialog.h"


class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QTcpSocket* socket;
    QByteArray socketData;
    QJsonArray ContactBook;
public slots:
    void sockReady();
    void sockDisc();
private:
    QWidget* mainWidget;
    QTableWidget* table;
    QPushButton* butAdd;
    QPushButton* butConnect;
    QPushButton* butDelete;
    QPushButton* butChange;
    QPushButton* butSave;
    QPushButton* butShow;
    QVBoxLayout* vlayout;
    QHBoxLayout* hlayout;
    void RefreshTable();
private slots:
    void slotAdd();
    void slotDelete();
    void slotConnect();
    void slotChange();
    void slotSave();
    void slotShow();
};

#endif // MAINWINDOW_H
