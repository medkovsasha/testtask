#ifndef CHANGEDIALOG
#define CHANGEDIALOG

#include <QLabel>
#include <QWidget>
#include <QDialog>
#include <QLineEdit>
#include <QGridLayout>
#include <QString>
#include <QPushButton>


class changeDialog : public QDialog
{
    Q_OBJECT
public:
    changeDialog(QWidget* pwgt = 0);
    QString getName() const;
    QString getnewName() const;
    QString getnewPhonenumber() const;
private:
    QLineEdit* m_ptxtname;
    QLineEdit* m_ptxtnewname;
    QLineEdit* m_ptxtnewphonenumber;
};

#endif // CHANGEDIALOG

