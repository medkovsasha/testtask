#ifndef INSERTDIALOG_H
#define INSERTDIALOG_H

#include <QLabel>
#include <QWidget>
#include <QDialog>
#include <QLineEdit>
#include <QGridLayout>
#include <QString>
#include <QPushButton>

class insertDialog : public QDialog
{
    Q_OBJECT
public:
    insertDialog(QWidget* pwgt = 0);
    QString getName() const;
    QString getNumber() const;
private:
    QLineEdit* m_ptxtname;
    QLineEdit* m_ptxtnumber;
};

#endif // INSERTDIALOG_H

