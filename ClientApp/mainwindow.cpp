#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    socket = new QTcpSocket(this);
    connect(socket,SIGNAL(readyRead()),this,SLOT(sockReady()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(sockDisc()));

    mainWidget = new QWidget(this);
    setCentralWidget(mainWidget);

    table = new QTableWidget(this);
    table->setColumnCount(2);
    table->setColumnWidth(0, 300);
    table->setColumnWidth(1, 300);
    table->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("NAME")));
    table->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("PHONE NUMBER")));
    table->setShowGrid(true);
    table->setSelectionMode(QAbstractItemView::SingleSelection);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);

    butAdd = new QPushButton("add");
    connect(butAdd, SIGNAL(clicked()), this, SLOT(slotAdd()));

    butConnect = new QPushButton("connect");
    connect(butConnect, SIGNAL(clicked()), this, SLOT(slotConnect()));

    butDelete = new QPushButton("delete");
    connect(butDelete, SIGNAL(clicked()), this, SLOT(slotDelete()));

    butChange = new QPushButton("change");
    connect(butChange, SIGNAL(clicked()), this, SLOT(slotChange()));

    butSave = new QPushButton("save");
    connect(butSave, SIGNAL(clicked()), this, SLOT(slotSave()));

    butShow = new QPushButton("show");
    connect(butShow, SIGNAL(clicked()), this, SLOT(slotShow()));

    vlayout = new QVBoxLayout;
    hlayout = new QHBoxLayout;
    vlayout->addWidget(table);
    hlayout->addWidget(butConnect);
    hlayout->addWidget(butShow);
    hlayout->addWidget(butAdd);
    hlayout->addWidget(butDelete);
    hlayout->addWidget(butChange);
    hlayout->addWidget(butSave);
    vlayout->addLayout(hlayout);
    mainWidget->setLayout(vlayout);
}

MainWindow::~MainWindow()
{
    delete(hlayout);
    delete(vlayout);
    delete(butAdd);
    delete(butConnect);
    delete(butSave);
    delete(butDelete);
    delete(butChange);
    delete(table);
    delete(mainWidget);
}

void MainWindow::RefreshTable()
{
    int n = table->rowCount();
    for(int i = 0; i < n; i++)
    {
        table->removeRow(0);
    }
    for(int i = 0; i < ContactBook.count(); i++)
    {
        table->insertRow(0);
        table->setItem(0, 0, new QTableWidgetItem(ContactBook[i].toObject().value("name").toString()));
        table->setItem(0, 1, new QTableWidgetItem(ContactBook[i].toObject().value("phonenumber").toString()));
        table->setRowHeight(0, 20);
    }
}

void MainWindow::sockReady()
{
    if(socket->waitForConnected(500))
    {
        socket->waitForReadyRead(500);
        socketData = socket->readAll();

        QJsonParseError docError;
        QJsonDocument doc = QJsonDocument::fromJson(socketData, &docError);

        if(docError.errorString() == "no error occurred")
        {
            if(doc.object().value("type").toString() == "connect" && doc.object().value("status").toString() == "yes")
            {
                QMessageBox::information(this, "Information", "Connect");
            }
            else if(doc.object().value("type").toString() == "data")
            {
                ContactBook = doc.object().value("result").toArray();
                RefreshTable();
            }
            else if(doc.object().value("type").toString() == "fileisempty")
            {
                QMessageBox::information(this, "Information", "Contact book is empty");
            }
            else
            {
                QMessageBox::information(this, "Information", "Error");
            }
        }
        else
        {
            QMessageBox::information(this, "Information", docError.errorString());
        }
    }
    else
    {
        QMessageBox::information(this, "Information", "No connection");
    }
}

void MainWindow::sockDisc()
{
    socket->deleteLater();
}

void MainWindow::slotConnect()
{
    socket->connectToHost("127.0.0.1", 5555);
    if(!socket->waitForConnected(500))
    {
        socket->close();
        QMessageBox::information(this, "Information", "Server is not available");
    }
}

void MainWindow::slotAdd()
{
    if(socket->isOpen())
    {
        insertDialog* pInsertDialog = new insertDialog;
        if(pInsertDialog->exec() == QDialog::Accepted)
        {
            QString Name = pInsertDialog->getName();
            QString Phonenumber = pInsertDialog->getNumber();
            if(Name != "" && Phonenumber != "")
            {
                if(ContactBook.count() < 5000)
                {
                    QVariantMap newContact;
                    newContact.insert("name", Name);
                    newContact.insert("phonenumber", Phonenumber);
                    QJsonObject newItem = QJsonObject::fromVariantMap(newContact);
                    ContactBook.push_back(newItem);
                }
                else
                {
                    QMessageBox::information(this, "Information", "contact count > 5000");
                }
            }
        }
        delete pInsertDialog;
        RefreshTable();
    }
    else
    {
        QMessageBox::information(this, "Information", "No connection");
    }
}

void MainWindow::slotDelete()
{
    if(socket->isOpen())
    {
        deleteDialog* pDeleteDialog = new deleteDialog;
        if(pDeleteDialog->exec() == QDialog::Accepted)
        {
            QString Name = pDeleteDialog->getName();
            if(Name != "")
            {
                for(int i = 0; i < ContactBook.count(); i++)
                {
                    if(ContactBook[i].toObject().value("name").toString() == Name)
                    {
                        ContactBook.removeAt(i);
                    }
                }
            }
        }
        delete pDeleteDialog;
        RefreshTable();
    }
    else
    {
        QMessageBox::information(this, "Information", "No connection");
    }
}

void MainWindow::slotChange()
{
    if(socket->isOpen())
    {
        changeDialog* pChangeDialog = new changeDialog;
        if(pChangeDialog->exec() == QDialog::Accepted)
        {
            QString Name = pChangeDialog->getName();
            QString newName = pChangeDialog->getnewName();
            QString newPhonenumber = pChangeDialog->getnewPhonenumber();
            if(Name != "" && newName != "" && newPhonenumber != "")
            {
                for(int i = 0; i < ContactBook.count(); i++)
                {
                    if(ContactBook[i].toObject().value("name").toString() == Name)
                    {
                        QVariantMap newContact;
                        newContact.insert("name", newName);
                        newContact.insert("phonenumber", newPhonenumber);
                        QJsonObject newItem = QJsonObject::fromVariantMap(newContact);
                        ContactBook[i] = newItem;
                    }
                }
            }
        }
        delete pChangeDialog;
        RefreshTable();
    }
    else
    {
        QMessageBox::information(this, "Information", "No connection");
    }
}

void MainWindow::slotSave()
{
    if(socket->isOpen())
    {
        QJsonDocument ito(ContactBook);
        QByteArray contacts = ito.toJson();
        QByteArray itog = "{\"type\":\"save\",\"result\":"+contacts+"}";
        socket->write(itog);
        socket->waitForBytesWritten(500);
    }
    else
    {
        QMessageBox::information(this, "Information", "No connection");
    }
}

void MainWindow::slotShow()
{
    if(socket->isOpen())
    {
        socketData = "{\"type\":\"takedata\"}";
        socket->write(socketData);
        socket->waitForBytesWritten(500);
    }
    else
    {
        QMessageBox::information(this, "Information", "No connection");
    }
}
